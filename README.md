# Startup turn on

## Description
configure the device:

```shell
sudo bash setup.sh
```

Following things will happen:
- All oldlines from crontab will be removed (user: pi)
- execute `main.py` on startup will be added to crontab

## How to run it
 - Make sure the device has an internet connection
 - Clone the repository with the following command:
 `git clone https://gitlab.com/optoceutics-public/startup-turn-on.git`
 - Go into directory with: `cd startup-turn-on`
 - Execute script with: `sudo bash startup.sh`
 - Now reboot