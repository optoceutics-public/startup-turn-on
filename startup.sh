SCRIPT_DIR=$(cd $(dirname "${BASH_SOURCE[0]}") && pwd)
CRONTAB_FILE="/var/spool/cron/crontabs/pi"

START_LINE="@reboot sudo python3 $SCRIPT_DIR/main.py"
REGEX=".*pi\/start\.sh.*"
SED_REGEX="/$REGEX$/d"

# Delete all existing lines matching regex
sed -i "$SED_REGEX" $CRONTAB_FILE

# Add line to crontab file
echo $START_LINE >> $CRONTAB_FILE